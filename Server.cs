﻿using StorageServer.WebSockets;
using System;

namespace StorageServer
{
    class Server
    {
        static void Main(string[] args)
        {
            WebSocketServer server = new WebSocketServer("0.0.0.0", 80);

            server.OnServerStarted += (object sender) => Console.WriteLine("Server started.");
            server.OnServerStopped += (object sender) => Console.WriteLine("Server stopped.");
            server.OnClientConnected += (object sender, ClientConnectedArgs e) => Console.WriteLine($"Client connected from address {e.GetIPAddress()}");
            server.OnMessageReceived += (object sender, MessageReceivedArgs e) => Console.WriteLine($"Client sent message {e.Message}");

            server.Start();

            Console.ReadLine();

            server.Stop();
        }
    }
}
