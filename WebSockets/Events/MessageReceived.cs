using System;
using System.Net;
using System.Net.Sockets;

namespace StorageServer.WebSockets
{
    public delegate void MessageReceivedHandler(object sender, MessageReceivedArgs e);
    public class MessageReceivedArgs : EventArgs
    {
        public TcpClient Client;
        public string Message;

        public MessageReceivedArgs(TcpClient client, string message)
        {
            Client = client;
            Message = message;
        }
    }
}