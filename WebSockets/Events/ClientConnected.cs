using System;
using System.Net;

namespace StorageServer.WebSockets
{
    public delegate void ClientConnectedHandler(object sender, ClientConnectedArgs e);
    public class ClientConnectedArgs : EventArgs
    {
        private IPAddress IpAddress;

        public ClientConnectedArgs(IPAddress address)
        {
            IpAddress = address;
        }

        public IPAddress GetIPAddress()
        {
            return IpAddress;
        }
    }
}