using System;
using System.Net;

namespace StorageServer.WebSockets
{
    public delegate void ClientDisconnectedHandler(object sender, ClientConnectedArgs e);
    public class ClientDisconnectedArgs : EventArgs
    {
        private IPAddress IpAddress;

        public ClientDisconnectedArgs(IPAddress address)
        {
            IpAddress = address;
        }

        public IPAddress GetIPAddress()
        {
            return IpAddress;
        }
    }
}