using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace StorageServer.WebSockets
{
    class WebSocketServer
    {
        private readonly string _ipAddress;
        private readonly int _port;
        private readonly TcpListener _server;

        private CancellationTokenSource connectionSource = new CancellationTokenSource();

        public event ClientConnectedHandler OnClientConnected;
        public event MessageReceivedHandler OnMessageReceived;
        public event ServerStartedHandler OnServerStarted;
        public event ServerStoppedHandler OnServerStopped;

        public WebSocketServer(string ipAddress, int port)
        {
            _ipAddress = ipAddress;
            _port = port;
            _server = new TcpListener(IPAddress.Parse(_ipAddress), _port);
        }

        public void Start()
        {
            foreach (ServerStartedHandler action in OnServerStarted.GetInvocationList())
            {
                action.Invoke(this);
            }

            _server.Start();

            var connectionThread = new Thread(() => ListenForConnections(connectionSource.Token));
            connectionThread.Start();
        }

        public void Stop()
        {
            foreach (ServerStoppedHandler action in OnServerStopped.GetInvocationList())
            {
                action.Invoke(this);
            }

            connectionSource.Cancel();
            _server.Stop();
        }

        private async void ListenForConnections(CancellationToken token)
        {
            CancellationTokenSource dataCancellationSource = new CancellationTokenSource();
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    dataCancellationSource.Cancel();
                    return;
                }
                TcpClient client = await _server.AcceptTcpClientAsync();
                Thread clientDataThread = new Thread(() => ListenForClientData(client, dataCancellationSource.Token));
                clientDataThread.Start();
                ClientConnectedArgs args = new ClientConnectedArgs(((IPEndPoint)client.Client.RemoteEndPoint).Address);
                foreach (ClientConnectedHandler action in OnClientConnected.GetInvocationList())
                {
                    action.Invoke(this, args);
                }
            }
        }

        private void ListenForClientData(TcpClient client, CancellationToken token)
        {
            NetworkStream stream = client.GetStream();
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    return;
                }
                while (client.Available < 3) { }
                byte[] bytes = new byte[client.Available];
                stream.Read(bytes, 0, bytes.Length);

                String data = Encoding.UTF8.GetString(bytes);

                if (Regex.IsMatch(data, "^GET"))
                {
                    const string eol = "\r\n";

                    string swk = Regex.Match(data, "Sec-WebSocket-Key: (.*)").Groups[1].Value.Trim();
                    string swka = swk + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
                    byte[] swkaSha1 = System.Security.Cryptography.SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(swka));
                    string swkaSha1Base64 = Convert.ToBase64String(swkaSha1);

                    byte[] response = Encoding.UTF8.GetBytes(
                        "HTTP/1.1 101 Switching Protocols" + eol +
                        "Connection: Upgrade" + eol +
                        "Upgrade: websocket" + eol +
                        "Sec-WebSocket-Accept: " + swkaSha1Base64 + eol + eol
                    );

                    stream.Write(response, 0, response.Length);
                }
                else
                {
                    bool fin = (bytes[0] & 0b10000000) != 0,
                        mask = (bytes[1] & 0b10000000) != 0;

                    int opcode = bytes[0] & 0b00001111,
                        msglen = bytes[1] - 128,
                        offset = 2;

                    if (msglen == 126)
                    {
                        msglen = BitConverter.ToUInt16(new byte[] { bytes[3], bytes[2] }, 0);
                        offset = 4;
                    }

                    if (msglen == 0)
                        continue;
                    else if (mask)
                    {
                        byte[] decoded = new byte[msglen];
                        byte[] masks = new byte[4] { bytes[offset], bytes[offset + 1], bytes[offset + 2], bytes[offset + 3] };
                        offset += 4;

                        for (int i = 0; i < msglen; ++i)
                            decoded[i] = (byte)(bytes[offset + i] ^ masks[i % 4]);
                        string text = Encoding.UTF8.GetString(decoded);

                        MessageReceivedArgs args = new MessageReceivedArgs(client, text);
                        foreach (MessageReceivedHandler action in OnMessageReceived.GetInvocationList())
                        {
                            action.Invoke(this, args);
                        }
                    }
                }
            }
        }
    }
}